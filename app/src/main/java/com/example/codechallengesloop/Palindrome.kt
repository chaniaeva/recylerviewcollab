package com.example.codechallengesloop

fun checkPalindrome(word : String) {
    var reversedWord = word.reversed()
    println(word.equals(reversedWord, ignoreCase = true))
}

fun main(){
    checkPalindrome("Aba")
}