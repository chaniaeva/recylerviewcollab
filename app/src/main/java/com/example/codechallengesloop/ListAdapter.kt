package com.example.codechallengesloop

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_number.view.*

class ListAdapter(var number : Int) : RecyclerView.Adapter<ListAdapter.ViewHolder> (){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var dataNumber = view.displayNumber
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemview = LayoutInflater.from(parent.context).inflate(R.layout.item_number, parent, false)
        return ViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var posisi : Int = position + 1
        if (posisi % 3 == 0 && posisi % 5 == 0) holder.dataNumber.text = "Boot Camp"
        else if (posisi % 3 == 0) holder.dataNumber.text = "Boot"
        else if (posisi % 5 == 0) holder.dataNumber.text = "Camp"
        else holder.dataNumber.text = posisi.toString()
    }

    override fun getItemCount(): Int {
        return number
    }
}